// Main program for Making Music with the Leap Motion Controller

//#include <vld.h>

#include "SineWave.h"
#include "RtAudio.h"
#include "Instrmnt.h"
#include "Leap.h"
#include <QApplication>
#include <QtGui>
#include <QWidget>
#include <QGroupBox>
#include <QVBoxLayout>
#include <QGridLayout>
#include <QPushButton>
#include <QLabel>
#include <QRadioButton>
#include <QButtonGroup>
#include <QCoreApplication>
#include <QComboBox>
#include <QMenu>
#include <QMenuBar>
#include <QGridLayout.h>
#include "mainWindow.h"
#include "helpWindow.h"

#include "BeeThree.h"
#include "HevyMetl.h"
#include "PercFlut.h"
#include "Rhodey.h"
#include "TubeBell.h"

double FREQUENCY = 0.0;
double VOLUME = 0.2;
bool PLAY = true;
int INSTRUMENT = 1;
int INSTRUMENT_CHOICE = 1;

using namespace stk;
// The following struct and function are modified versions of the function taken from the instruments tutorial provided by Perry R. Cook and Gary P. Scavone
// available at https://ccrma.stanford.edu/software/stk/instruments.html

struct TickData {
	Instrmnt* instrument;
	StkFloat frequency;
	StkFloat scaler;
	long counter;
	bool done;

	TickData()
		: instrument(0), scaler(1.0), counter(0), done(false) {}
};


// This tick() function handles sample computation only.  It will be
// called automatically when the system needs a new buffer of audio
// samples.
int tick(void* outputBuffer, void* inputBuffer, unsigned int nBufferFrames,
	double streamTime, RtAudioStreamStatus status, void* userData)
{
	TickData* data = (TickData*)userData;
	register StkFloat* samples = (StkFloat*)outputBuffer;
	for (unsigned int i = 0; i < nBufferFrames; i++) {
		*samples++ = data->instrument->tick();
	}
	if (data->counter > 80000)
		data->done = true;
	return 0;
}


MainWindow::MainWindow(QWidget* parent)
	: QMainWindow(parent)
{
	QWidget* displayWindow = new QWidget;
	QPushButton* help_button = new QPushButton("New user? Click here for help!", displayWindow);
	QPushButton* start_stream_button = new QPushButton("Start Instrument", displayWindow);
	QPushButton* stop_stream_button = new QPushButton("Stop Instrument", displayWindow);
	QPushButton* quit_button = new QPushButton("Quit", displayWindow);
	QLabel* instrument_options_label = new QLabel(displayWindow);
	QComboBox* instrument_options = new QComboBox(displayWindow);
	QRadioButton* inst_flute = new QRadioButton("Flute", displayWindow);
	QRadioButton* inst_guitar = new QRadioButton("Electric Guitar", displayWindow);
	QRadioButton* inst_bee = new QRadioButton("Buzz", displayWindow);
	QRadioButton* inst_bell = new QRadioButton("Bell", displayWindow);

	QLabel* display_label = new QLabel(displayWindow);

	QAction* help = new QAction("&Help", this);
	QAction* quit1 = new QAction("&Quit", this);

	QMenu* file;

	file = menuBar()->addMenu("&File");
	file->addAction(help);
	file->addAction(quit1);


	instrument_options_label->setText("Instrument Type:");
	instrument_options->addItem("Theremin");
	instrument_options->addItem("Keyboard");

	display_label->setText("Welcome to Making Music with Leap Motion!");
	QGridLayout* mainLayout = new QGridLayout;
	
	// instrument radio button selectors
	QGroupBox* instrumentGroup = new QGroupBox(tr("Instruments"));
	QVBoxLayout* layout = new QVBoxLayout;
	layout->addWidget(inst_flute, 1);
	inst_flute->setChecked(true);
	inst_flute->setToolTip("Set instrument to Flute");
	layout->addWidget(inst_guitar, 1);
	inst_guitar->setToolTip("Set instrument to Electric Guitar");
	layout->addWidget(inst_bell, 1);
	inst_bell->setToolTip("Set instrument to Bell");
	layout->addWidget(inst_bee, 1);
	inst_bee->setToolTip("Set instrument to Buzz");
	instrumentGroup->setLayout(layout);
	QButtonGroup* instChoice = new QButtonGroup();
	instChoice->addButton(inst_flute, 1);
	instChoice->addButton(inst_guitar, 2);
	instChoice->addButton(inst_bell, 3);
	instChoice->addButton(inst_bee, 4);

	// instrument type drop down selection
	QGroupBox* typeGroup = new QGroupBox();
	QGridLayout* layout2 = new QGridLayout();
	layout2->addWidget(instrument_options_label, 1,1);
	layout2->addWidget(instrument_options, 1, 2);
	instrument_options->setToolTip("Set instrument type");
	typeGroup->setLayout(layout2);

	// bottom bar buttons
	QGroupBox* buttonsGroup = new QGroupBox();
	QHBoxLayout* layout3 = new QHBoxLayout;
	layout3->addWidget(start_stream_button, 1);
	start_stream_button->setToolTip("Begin playing the instrument");
	layout3->addWidget(stop_stream_button, 1);
	stop_stream_button->setToolTip("Stop the instrument output");
	layout3->addWidget(quit_button, 1);
	quit_button->setToolTip("Close the application");
	buttonsGroup->setLayout(layout3);

	QGroupBox* frequencyGroup = new QGroupBox();
	QGridLayout* layout4 = new QGridLayout();
	layout4->addWidget(display_label, 1, 1);
	frequencyGroup->setLayout(layout4);

	// add layout sections to main window
	mainLayout->addWidget(help_button, 0,0);
	help_button->setStyleSheet("font:italic 20px;");
	help_button->setFlat(true);
	mainLayout->addWidget(instrumentGroup, 1, 0);
	mainLayout->addWidget(typeGroup, 1, 1);
	mainLayout->addWidget(frequencyGroup, 0, 1);
	frequencyGroup->resize(200, frequencyGroup->height());
	mainLayout->addWidget(buttonsGroup, 2, 1);
	displayWindow->setLayout(mainLayout);

	this->setCentralWidget(displayWindow);

	(void)connect(start_stream_button, SIGNAL(released()), this, SLOT(startStream()));
	(void)connect(stop_stream_button, SIGNAL(released()), this, SLOT(stopPlay()));
	(void)connect(quit_button, SIGNAL(released()), this, SLOT(quit()));
	(void)connect(instrument_options, SIGNAL(currentIndexChanged(const QString&)), this, SLOT(changeInstrumentType(QString)));
	(void)connect(instChoice, SIGNAL(buttonClicked(int)), this, SLOT(changeInstrument(int)));
	(void)connect(help, SIGNAL(triggered()), this, SLOT(helpWindow()));
	(void)connect(help_button, SIGNAL(released()), this, SLOT(helpWindow()));
	(void)connect(quit1, SIGNAL(triggered()), this, SLOT(quit()));
}


void MainWindow::startStream()
{
	// Set the global sample rate before creating class instances.
	Stk::setSampleRate(44100.0);
	TickData data;
	RtAudio dac;
	// Figure out how many bytes in an StkFloat and setup the RtAudio stream.
	RtAudio::StreamParameters parameters;
	parameters.deviceId = dac.getDefaultOutputDevice();
	parameters.nChannels = 1;
	RtAudioFormat format = (sizeof(StkFloat) == 8) ? RTAUDIO_FLOAT64 : RTAUDIO_FLOAT32;
	unsigned int bufferFrames = RT_BUFFER_SIZE;

	try
	{
		dac.openStream(&parameters, NULL, format, (unsigned int)Stk::sampleRate(), &bufferFrames, &tick, (void*)&data);
	}
	catch (RtAudioError & error)
	{
		error.printMessage();
		return;
	}
	if (data.instrument)
	{
		delete data.instrument;
	}
	// Set instrument choice
	if (INSTRUMENT_CHOICE == 1) {
		try {
			data.instrument = new PercFlut();

		}
		catch (StkError&) {
			return;
		}
	}
	else if (INSTRUMENT_CHOICE == 2) {
		try {
			data.instrument = new HevyMetl();

		}
		catch (StkError&) {
			return;
		}
	}
	else if (INSTRUMENT_CHOICE == 3) {
		try {
			data.instrument = new TubeBell();

		}
		catch (StkError&) {
			return;
		}
	}
	else if (INSTRUMENT_CHOICE == 4) {
		try {
			data.instrument = new BeeThree();

		}
		catch (StkError&) {
			return;
		}
	}
	// start the stream
	try
	{
		dac.startStream();
		PLAY = true;
	}

	catch (RtAudioError & error)
	{
		error.printMessage();
		return;
	}
	while (PLAY)
	{
		// get current frequency and volume and adjust the note playing accordingly
		data.instrument->noteOn(FREQUENCY, VOLUME);
		// check for gui events to ensure application stays responsive
		QCoreApplication::processEvents();
	}
	delete data.instrument;
}


void MainWindow::stopPlay()
{
	PLAY = false;
}

// instrument type selector
void MainWindow::changeInstrumentType(QString inst)
{
	if (PLAY == true)
	{
		stopPlay();
	}
	if (inst.compare("Theremin"))
	{
		INSTRUMENT = 2;
	}
	else if (inst.compare("Keyboard"))
	{
		INSTRUMENT = 1;
	}
}

void MainWindow::changeInstrument(int choice) {
	// if currently playing, stop the instrument
	if (PLAY == true)
	{
		stopPlay();
	}
	if (choice == 1) 
	{
		INSTRUMENT_CHOICE = 1;
	}
	else if (choice == 2)
	{
		INSTRUMENT_CHOICE = 2;
	}
	else if (choice == 3)
	{
		INSTRUMENT_CHOICE = 3;
	}
	else if (choice == 4)
	{
		INSTRUMENT_CHOICE = 4;
	}
};

// close the main window
void MainWindow::quit()
{
	stopPlay();
	QCoreApplication::quit();
}

// show help menu
void MainWindow::helpWindow()
{
	HelpWindow helpWindow = new HelpWindow(this);
	helpWindow.setModal(true);
	helpWindow.resize(800, 1000);
	helpWindow.exec();
}

// Mimic a keyboard, with frequency jumps when the right hand changes x-position
void keyboard(Leap::HandList hands, int rhand, int lhand)
{
	if (hands[rhand].grabStrength() > 0.1 || hands[rhand].palmPosition()[0] == 0)
		FREQUENCY = 0;
	else {
		// right hand y-position controls volume
		VOLUME = (hands[lhand].palmPosition()[1] - 100) / (400 - 100);
		if (hands[rhand].palmPosition()[0] < -325 || hands[rhand].palmPosition()[0] > 300)
			FREQUENCY = 0;
		// left hand x-position controls key played, from C3 to C5
		else if (hands[rhand].palmPosition()[0] <= -300)
			FREQUENCY = 130;
		else if (hands[rhand].palmPosition()[0] <= -275)
			FREQUENCY = 138;
		else if (hands[rhand].palmPosition()[0] <= -250)
			FREQUENCY = 146;
		else if (hands[rhand].palmPosition()[0] <= -225)
			FREQUENCY = 155;
		else if (hands[rhand].palmPosition()[0] <= -200)
			FREQUENCY = 164;
		else if (hands[rhand].palmPosition()[0] <= -175)
			FREQUENCY = 174;
		else if (hands[rhand].palmPosition()[0] <= -150)
			FREQUENCY = 184;
		else if (hands[rhand].palmPosition()[0] <= -125)
			FREQUENCY = 195;
		else if (hands[rhand].palmPosition()[0] <= -100)
			FREQUENCY = 207;
		else if (hands[rhand].palmPosition()[0] <= -75)
			FREQUENCY = 220;
		else if (hands[rhand].palmPosition()[0] <= -50)
			FREQUENCY = 233;
		else if (hands[rhand].palmPosition()[0] <= -25)
			FREQUENCY = 246;
		// C4 (middle C)
		else if (hands[rhand].palmPosition()[0] <= 0)
			FREQUENCY = 261;
		else if (hands[rhand].palmPosition()[0] <= 25)
			FREQUENCY = 277;
		else if (hands[rhand].palmPosition()[0] <= 50)
			FREQUENCY = 293;
		else if (hands[rhand].palmPosition()[0] <= 75)
			FREQUENCY = 311;
		else if (hands[rhand].palmPosition()[0] <= 100)
			FREQUENCY = 329;
		else if (hands[rhand].palmPosition()[0] <= 125)
			FREQUENCY = 349;
		else if (hands[rhand].palmPosition()[0] <= 150)
			FREQUENCY = 369;
		else if (hands[rhand].palmPosition()[0] <= 175)
			FREQUENCY = 391;
		else if (hands[rhand].palmPosition()[0] <= 200)
			FREQUENCY = 415;
		else if (hands[rhand].palmPosition()[0] <= 225)
			FREQUENCY = 440;
		else if (hands[rhand].palmPosition()[0] <= 250)
			FREQUENCY = 466;
		else if (hands[rhand].palmPosition()[0] <= 275)
			FREQUENCY = 493;
		else if (hands[rhand].palmPosition()[0] <= 300)
			FREQUENCY = 523;
	}
}

void theremin(Leap::HandList hands)
{
	// Discover hands to ensure left and right hand controls are set correctly
	if (hands[0].isLeft())
	{
		// Check if hands are out of range of Leap device
		if (hands[0].palmPosition()[1] == 0 || hands[1].palmPosition()[0] == 0)
		{
			FREQUENCY = 0;
		}
		else {
			if (hands[0].palmPosition()[1] < 100 || hands[0].palmPosition()[1] >= 400) {
				FREQUENCY = 0;
			}
			else {
				if (hands[1].palmPosition()[0] < -325 || hands[1].palmPosition()[0] > 300)
					FREQUENCY = 0;
				// left hand y-pos controls volume, right hand x-pos controls pitch
				VOLUME = (hands[0].palmPosition()[1] - 100) / (400 - 100);
				FREQUENCY = (int)hands[1].palmPosition()[0] + 300;
			}
		}
	}
	else if (hands[1].isLeft())
	{
		// Left hand y-pos controls volume, right hand x-pos controls pitch

		if (hands[0].palmPosition()[0] == 0 || hands[1].palmPosition()[0] == 0)
		{
			FREQUENCY = 0;
		}
		else {
			if (hands[1].palmPosition()[1] < 100 || hands[1].palmPosition()[1] >= 400)
			{
				FREQUENCY = 0;
			}
			else
			{
				if (hands[0].palmPosition()[0] < -325 || hands[0].palmPosition()[0] > 300)
					FREQUENCY = 0;
				// normalise left hand position 

				VOLUME = (hands[1].palmPosition()[1] - 100) / (400 - 100);
				FREQUENCY = (int)hands[0].palmPosition()[0] + 300;
			}
		}
	}

}

void setFreq(Leap::HandList hands)
{
	// Set the instrument type to be played
	if (INSTRUMENT == 1)
	{
		theremin(hands);
	}
	else if (INSTRUMENT == 2)
	{
		FREQUENCY = 0;
		if (hands[0].isRight())
		{
			keyboard(hands, 0, 1);
		}
		else if (hands[1].isRight())
		{
			keyboard(hands, 1, 0);
		}
	}

}

class SampleListener : public Leap::Listener {
public:
	virtual void onFrame(const Leap::Controller&);
};

void SampleListener::onFrame(const Leap::Controller& controller)
{
	Leap::Frame frame = controller.frame();
	Leap::HandList hands = frame.hands();

	setFreq(hands);
}


int main(int argc, char* argv[])
{
	SampleListener listener;
	Leap::Controller controller;
	controller.addListener(listener);

	// set GUI 
	QApplication app(argc, argv);
	MainWindow window;
	window.resize(800, 640);
	window.show();
	window.setWindowTitle(
		QApplication::translate("toplevel", "Making Music with Leap Motion"));
	// start the application
	app.exec();

	controller.removeListener(listener);
	return 0;
}


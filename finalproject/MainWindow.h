#pragma once

#include <QMainWindow>
#include <QPushButton>
#include "RtAudio.h"

namespace Ui {
	class MainWindow;
}

class MainWindow : public QMainWindow
{
	Q_OBJECT
public:
	explicit MainWindow(QWidget* parent = 0);
private slots:
	void startStream();
	void changeInstrumentType(QString inst);
	void changeInstrument(int choice);
	void helpWindow();
	void stopPlay();
	void quit();
private:
	QPushButton* start_stream_button;
	QPushButton* stop_stream_button;
	QPushButton* quit_button;
};
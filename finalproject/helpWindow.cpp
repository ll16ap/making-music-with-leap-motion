#include <QtGui>
#include <QHBoxLayout>
#include <QMessageBox>
#include <QPixmap>
#include <QLabel>

#include "helpWindow.h"

HelpWindow::HelpWindow(QWidget* parent)
	: QDialog(parent)
{
	// load image for page
	QPixmap pix("help_info.png");

	closeButton = new QPushButton(tr("Close"));
	(void)connect(closeButton, SIGNAL(clicked()), this, SLOT(close()));

	// add image to label
	QLabel* helpLabel = new QLabel(this);
	helpLabel->setFixedHeight(1300);
	helpLabel->setFixedWidth(1000);
	int w = helpLabel->width();
	int h = helpLabel->height();
	helpLabel->setPixmap(pix.scaled(w,h,Qt::KeepAspectRatio));
	
	QVBoxLayout* textLayout = new QVBoxLayout;
	QGridLayout* mainLayout = new QGridLayout;

	// add widgets to layout
	mainLayout->addWidget(helpLabel, 0, 0);
	mainLayout->addWidget(closeButton, 1, 0);
	closeButton->setFixedWidth(200);
	setLayout(mainLayout);
}


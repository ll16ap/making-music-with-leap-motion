#pragma once

#include <QDialog>
#include <QMainWindow>
#include <QPushButton>
#include <QLabel>

namespace Ui {
	class MainWindow;
}

class HelpWindow : public QDialog
{
	Q_OBJECT

public:
	HelpWindow(QWidget* parent = 0);

private:
	QPushButton* closeButton;
	QLabel* helpLabel;
};


Welcome to Making Music with the Leap Motion Controller.

This project is a piece of exploratory software developed for 
a third year project at the University of Leeds.

The development of the software is documented in a report submitted 
to the University.

The software is not meant to be distributed and is only meant 
as a demonstration of the viability of technologies.

Build Instructions:

The project must be run in a Windows environment.
The Leap SDK 2.3 must be installed on the machine, it may run with 3.2 
has not been tested.
The Synthesis Toolkit library must be installed.
To run the project, is to download the project folder and
open the finalproject.vcxproj file to open and run the project in Visual Studio.
